<?php

namespace Patterns;


use Exception;
use Traversable;

class Block implements RelationsInterface, \IteratorAggregate
{
    public $name = 'BaseBlock';
    private $parent;
    private $childrens = [];
    public $id;
    public $type;


    public function render()
    {
        echo sprintf('<%s> </%s>', $this->name, $this->name);
    }

    public function renderComposition()
    {
        $childrenContent = '';
        /** @var RelationsInterface $child */
        foreach ($this->childrens as $child) {
            $childrenContent .= $child->renderComposition();
        }
        $content = sprintf('<%s>%s</%s>', $this->name, $childrenContent, $this->name);

        //если есть родитель - возвращаемб если нет - рендерим
        if ($this->parent === null) {
            echo $content;
        } else {
            return $content;
        }
    }

    public function getLength()
    {
        return strlen(sprintf('<%s> </%s>', $this->name, $this->name));
    }

    public function setParent(RelationsInterface $parentBlock)
    {
        $this->parent = $parentBlock;
    }

    public function addChild(RelationsInterface $childBlock)
    {
        $this->childrens[] = $childBlock;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getChildren()
    {
        return $this->childrens;
    }

    public function getIterator(): \Iterator
    {
        return new RecIterator($this);
    }

    public function getReverseIterator(): \Iterator
    {
        return new RecIterator($this, true);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function getBlockName()
    {
        return $this->name;
    }
}
