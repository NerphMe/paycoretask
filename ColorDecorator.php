<?php
namespace Patterns;


class ColorDecorator
{
    /**
     * @var string
     */
    private string $color;
    /**
     * @var int
     */
    private int $borderdWidth;

    public function __construct(string $color, int $borderdWidth)
    {

        $this->color = $color;
        $this->borderdWidth = $borderdWidth;
    }

    public function decorate(RenderableInterface $renderableBlock)
    {
        $decoratedContent = '';
        $decoratedContent .= sprintf('<div style=" border: %spx solid %s">', $this->borderdWidth, $this->color);
        ob_start();
        $renderableBlock->render();
        $decoratedContent .= ob_get_clean();
        $decoratedContent .= '</div>';
        echo $decoratedContent;
    }


}