<?php

namespace Patterns;


class CommentDecorator
{
    public function decorate(RenderableInterface $renderableBlock)
    {
        //Оборачмваем в див , для коректоного отображения коментариев в коде страницы.
        $decoratedContent = '<div>';
        $decoratedContent .= sprintf('<!-- Block BEGIN. Type: %s, ID: %s , Length: %s -->', get_class($renderableBlock), spl_object_id($renderableBlock), $renderableBlock->getLength());
        ob_start();
        $renderableBlock->render();
        $decoratedContent .= ob_get_clean();
        $decoratedContent .= sprintf('<!-- Block End. Type: %s, ID: %s , Length: %s -->', get_class($renderableBlock), spl_object_id($renderableBlock), $renderableBlock->getLength());
        echo $decoratedContent;
    }
}