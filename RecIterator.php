<?php


namespace Patterns;


class RecIterator
{

    public function iterate(RelationsInterface $block, int $counter = 0)
    {
        /** @var Block $blockChild */
        foreach ($block->getChildren() as $blockChild) {
            $counter++;
            $block->setId($counter);
            $block->setType($blockChild->getBlockName());

            if (count($blockChild->getChildren()) > 0) {
               $counter = $this->iterate($blockChild, $counter);
            }
        }

        return $counter;
    }

}