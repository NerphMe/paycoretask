<?php


namespace Patterns;


interface RelationsInterface extends RenderableInterface
{
    public function setParent(RelationsInterface $parentBlock);

    public function addChild(RelationsInterface $childBlock);

    public function getParent();

    public function getChildren();

    public function renderComposition();
}