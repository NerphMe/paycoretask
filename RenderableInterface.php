<?php


namespace Patterns;


interface RenderableInterface
{
    public function render();
}
