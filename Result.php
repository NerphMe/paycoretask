<?php
namespace Patterns;

require __DIR__ . '/vendor/autoload.php';

//$dec = new ColorDecorator('black', 100);
//$img = new ImageBlock();
//$dec->decorate($img);

$baseBlock = new Block();
$textBlock1 = new TextBlock();
$textBlock2 = new TextBlock();
$buttonBlock3 = new ButtonBlock();
$buttonBlock4 = new ButtonBlock();
$textBlock5 = new TextBlock();
$textBlock6 = new TextBlock();
$imageBlock = new ImageBlock();

$textBlock1->addChild($textBlock2);
$textBlock2->addChild($buttonBlock3);
$textBlock2->addChild($buttonBlock4);
$textBlock5->addChild($textBlock6);

$textBlock2->setParent($textBlock1);
$buttonBlock3->setParent($textBlock2);
$buttonBlock4->setParent($textBlock2);
$textBlock6->setParent($textBlock5);

$baseBlock->addChild($textBlock1);
$baseBlock->addChild($textBlock5);
$baseBlock->addChild($imageBlock);

$textBlock1->setParent($baseBlock);
$textBlock5->setParent($baseBlock);
$imageBlock->setParent($baseBlock);

//$baseBlock->renderComposition();
$iterator = new RecIterator();
$iterator->iterate($baseBlock);
//$baseBlock->renderComposition();
var_dump($baseBlock);
