<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit9d75faa8e37692aad52f5cf1a6f65eb6
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'Patterns\\' => 9,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Patterns\\' => 
        array (
            0 => __DIR__ . '/../..' . '/',
        ),
    );

    public static $classMap = array (
        'Patterns\\Block' => __DIR__ . '/../..' . '/Block.php',
        'Patterns\\ButtonBlock' => __DIR__ . '/../..' . '/ButtonBlock.php',
        'Patterns\\ColorDecorator' => __DIR__ . '/../..' . '/ColorDecorator.php',
        'Patterns\\CommentDecorator' => __DIR__ . '/../..' . '/CommentDecorator.php',
        'Patterns\\ImageBlock' => __DIR__ . '/../..' . '/ImageBlock.php',
        'TextBlock' => __DIR__ . '/../..' . '/TextBlock.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit9d75faa8e37692aad52f5cf1a6f65eb6::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit9d75faa8e37692aad52f5cf1a6f65eb6::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit9d75faa8e37692aad52f5cf1a6f65eb6::$classMap;

        }, null, ClassLoader::class);
    }
}
